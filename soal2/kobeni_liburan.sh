#!/bin/bash

# direktori tempat file sh berada
dir=$PWD/kobeni_liburan.sh

# deklarasi counter temp1 untuk nama folder yg didownload
temp1=1
# cek ada/tidak folder yang akan dieksekusi
while [ -d "kumpulan_$temp1" ]
do temp1=$((temp1+1))
done

kumpulanDownload() {
    # penyelesaian problem 1
    if [[ ! -d "kumpulan_$temp1" ]]; then
        # buat folder baru
        mkdir "kumpulan_$temp1"
        # cek jam terkini
        hour=$(date +"%H")
        # looping sesuai waktu
        for ((i=1; i<=$hour; i++))
        do 
            # download gambar random dan disimpan dalam folder baru
            wget -O "kumpulan_$temp1/perjalanan_$i.jpg" "https://source.unsplash.com/random/800x600/?indonesia"
        done
    fi
}

devilZip() {
    # deklarasi counter temp1 untuk nama folder yg didownload
    temp2=1
    # cek ada/tidak folder yang akan dieksekusi
    while [ -f "devil_$temp2.zip" ]
    do temp2=$((temp2+1))
    done

    # penyelesaian problem 2
    if [[ ! -d "devil_$temp2.zip" ]]; then
        kumpulanTemp=1
        while [ -d "kumpulan_$kumpulanTemp" ]
        do 
        # kompress folder baru
        zip -r "devil_$temp2.zip" "kumpulan_$kumpulanTemp"
        # hapus folder baru yang tidak dikompress
        rm -rf "kumpulan_$kumpulanTemp"
        kumpulanTemp=$((kumpulanTemp+1))
        done
    fi
}

if [ ! $1 ]; then
    echo "Download Image atau Zip?"
    echo "1 Untuk Download Image"
    echo "2 Untuk Zip"
    exit
fi

if [ $1 -eq 1 ]; then
    kumpulanDownload
elif [ $1 -eq 2 ]; then
    devilZip
fi

# Crontab
# Cron Job Download Image
# * */10 * * * bash $pwd/kobeni_liburan.sh 1
# Cron Job Zip
# 0 0 * * * bash $pwd/kobeni_liburan.sh 2
