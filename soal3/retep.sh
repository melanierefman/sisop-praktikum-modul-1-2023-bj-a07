#!/bin/bash

# take user_name input from the user
echo -n "Enter username: "
read user_name

# take user_password input from the user
echo -n "Enter password: "
read -s user_password
echo ""

# check whether the user_name and user_password are registered or not
if grep -q "^$user_name:$user_password" /users/users.txt; then
	echo "$(date +"%y/%m/%d %H:%M:%S") LOGIN: INFO User $user_name logged in" >> log.txt
	echo "> Successfully logged in. Welcome, $user_name!"
else
	echo "$(date +"%y/%m/%d %H:%M:%S") LOGIN: ERROR Failed login attempt on user $user_name" >> log.txt
	echo "> Login failed. Make sure your username and password are correct"
fi