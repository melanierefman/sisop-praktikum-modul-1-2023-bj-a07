#!/bin/bash

# make /users/users.txt in the root directory if it doesn't exist
if [ ! -d /users ]; then
    mkdir /users
fi
if [ ! -f /users/users.txt ]; then
    touch /users/users.txt
fi

# take user_name input from the user
echo -n "Enter new username: "
read user_name

# check if the user_name is already registered and loop until the user_name is valid
while grep -q "^$user_name:" /users/users.txt; do
    echo "$(date +"%y/%m/%d %H:%M:%S") REGISTER: ERROR User already exists" >> log.txt
    echo "> Username already registered"
    echo -n "Enter new username: "
    read user_name
done

# take user_password input from the user
echo -n "Enter new password: "

# check whether the user_password fulfills the requirements and loop until it does
while true; do
    # read the user_password without echoing it
    read -s user_password
    echo ""

    # initialize valid checker variable to true
    is_valid=true

    # password requirements checkings
    if [ ${#user_password} -lt 8 ]; then
        echo "> Password must be at least 8 characters long"
        is_valid=false
    fi
    if ! [[ "$user_password" =~ [[:upper:]] ]] || ! [[ "$user_password" =~ [[:lower:]] ]] || ! [[ "$user_password" =~ [[:digit:]] ]]; then
        echo "> Password must contain at least 1 uppercase letter, 1 lowercase letter, and 1 number"
        is_valid=false
    fi
    if ! [[ "$user_password" =~ ^[[:alnum:]]+$ ]]; then
        echo "> Password must contain only alphanumeric characters"
        is_valid=false
    fi
    if [ "$user_password" == "$user_name" ]; then
        echo "> Password must not be the same as the username"
        is_valid=false
    fi
    if [[ "$user_password" =~ [Cc][Hh][Ii][Cc][Kk][Ee][Nn] ]] || [[ "$user_password" =~ [Ee][Rr][Nn][Ii][Ee] ]]; then
        echo "> Password must not contain the word: chicken or ernie"
        is_valid=false
    fi

    # break the loop if the user_password fulfills the requirements
    if [ "$is_valid" = true ]; then
        break
    fi

    # ask the user to input the user_password again if it doesn't fulfill all the requirements
    echo -n "Enter new password: "
done

# add the user_name and user_password to the /users/users.txt file
echo "$user_name:$user_password" >> /users/users.txt

# print the success message to the log file and the terminal
echo "$(date +"%y/%m/%d %H:%M:%S") REGISTER: INFO User $user_name registered successfully" >> log.txt
echo "> User registered successfully."