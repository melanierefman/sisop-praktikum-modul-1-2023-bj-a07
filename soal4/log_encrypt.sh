#!/bin/bash

# Membuat directory baru untuk menyimpan backup log file
sudo mkdir encrypt_logs

# Deklarasi nama file .txt sesuai dengan ketentuan di soal yaitu 'jam:menit tanggal:bulan:tahun.txt'
temp="$(date +"%H:%M %d:%m:%y").txt"

# Membuat file .txt baru
sudo touch encrypt_logs/"$temp"
# Deklarasi jam sekarang untuk menentukan berapa kali huruf akan di geser
hour=$(date +"%H")
# Deklarasi semua alphabet huruf kecil
alphabetLower="abcdefghijklmnopqrstuvwxyz"
# Deklarasi semua alphabet huruf besar
alphabetUpper="ABCDEFGHIJKLMNOPQRSTUVWXYZ"
# Mengambil semua isi file di syslog kemudian di shift sesuai jam sekarang dan masukkan kedalam file.txt yang telah dibuat
$(cat /var/log/syslog | tr "${alphabetLower:0:26}" "${alphabetLower:$hour:26}${alphabetLower:0:$hour}" |  tr "${alphabetUpper:0:26}" "${alphabetUpper:$hour:26}${alphabetUpper:0:$hour}" > "encrypt_logs/$temp")

# Crontab
# 0 */2 * * * /bin/bash /home/rencist/Downloads/Praktikum1/sisop-praktikum-modul-1-2023-bj-a07/soal4/log_encrypt.sh