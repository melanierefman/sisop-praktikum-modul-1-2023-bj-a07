#!/bin/bash

# Membuat directory baru untuk menyimpan semua file hasil decrypt
sudo mkdir decrypt_logs
# Pergi ke dalam folder encrypt untuk mendapatkan konten file .txt
cd encrypt_logs

# Gunakan looping untuk setiap isi file .txt di dalam folder encrypt
for file in *.txt; do
    # Keluar dari file encrypt
    cd ..
    # Buat file baru untuk menyimpan hasil decrypt sesuai dengan nama file encrypt
    sudo touch decrypt_logs/"$file"
    # Deklarasi jam sesuai file encrypt
    hour=$(echo $file | cut -c-2)
    # Deklarasi alphabet huruf kecil
    alphabetLower="abcdefghijklmnopqrstuvwxyz"
    # Deklarasi alphabet huruf besar
    alphabetUpper="ABCDEFGHIJKLMNOPQRSTUVWXYZ"
    # Mengambil semua isi file encrypt kemudian di shift secara terbalik dan dimasukkan kedalam file decrypt
    decrypt=$(cat encrypt_logs/"$file" | tr "${alphabetLower:$hour:26}${alphabetLower:0:$hour}" "${alphabetLower:0:26}" |  tr "${alphabetUpper:$hour:26}${alphabetUpper:0:$hour}" "${alphabetUpper:0:26}" > "decrypt_logs/$file")
    # Masuk kedalam file decrypt
    cd decrypt_logs
done

