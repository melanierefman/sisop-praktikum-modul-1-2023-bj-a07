# Praktikum Sisop Modul 1 Kelompok A07

Anggota Kelompok A07:
| Nama | NRP |
| ---------------------- | ---------- |
| Melanie Sayyidina Sabrina Refman | 5025211029 |
| Akmal Sulthon Fathulloh | 5025211047 |
| Muhammad Dafian Zakiakhdan | 5025211108 |

----

## Soal 1

Bocchi hendak melakukan University Admission Test di Jepang. Bocchi ingin masuk ke universitas yang bagus. Akan tetapi, dia masih bingung sehingga ia memerlukan beberapa strategi untuk melakukan hal tersebut. Untung saja Bocchi menemukan file .csv yang berisi ranking universitas dunia untuk melakukan penataan strategi :

### 1.1 5 Universitas dengan ranking tertinggi di Jepang

Bocchi ingin masuk ke universitas yang bagus di Jepang. Oleh karena itu, Bocchi perlu melakukan survey terlebih dahulu. Tampilkan 5 Universitas dengan ranking tertinggi di Jepang.

**Code**

```sh
awk '/Japan/ {print}' 2023\ QS\ World\ University\ Rankings.csv | cut -d ',' -f 2 | head -n 5
```

- `awk '/Japan/ {print}' 2023\ QS\ World\ University\ Rankings.csv` Pertama gunakan awk untuk mendapatkan content di file yang hanya mengandung kata `Japan`
- `cut -d ',' -f 2` setelah itu gunakan pipeline `|` dan lanjutkan menggunakan cut yang memisahkan elemen berdasarkan `,` dan `-f 2` yang artinya kita hanya mengambil semua elemen pada kolom 2
- `head -n 5` gunakan pipeline `|` dan lanjutkan menggunakan `head -n 5` untuk mendapatkan 5 elemen teratas

**Output**

![image](https://user-images.githubusercontent.com/91055469/224313170-56d26dd1-0bd9-470d-a75a-15f783aa4dd4.png)

<br />

### 1.2 5 Universitas dengan FSR Score paling rendah di Jepang

Karena Bocchi kurang percaya diri dengan kemampuannya, coba cari Faculty Student Score(fsr score) yang paling rendah diantara 5 Universitas di Jepang.

**Code**

```sh
awk '/Japan/ {print}' 2023\ QS\ World\ University\ Rankings.csv | awk -F ',' '{print $9,$2}' | sort -n | head -5 | cut -d ' ' -f2-
```

- `awk '/Japan/ {print}' 2023\ QS\ World\ University\ Rankings.csv` Pertama gunakan awk untuk mendapatkan content di file yang hanya mengandung kata `Japan`
- `awk -F ',' '{print $9,$2}'` setelah gunakan pipeline `|` gunakan `awk` untuk memisahkan indeks berdasarkan `,` dan gunakan `{print $9, $2}` untuk mendapatkan hanya kolom ke - 9 dan kolom ke - 2
- `sort -n` gunakan sort untuk mengurutkan berdasarkan indeks pertama yang di print yaitu indeks ke - 9
- `head -5` gunakan head untuk mendapatkan 5 elemen teratas
- `cut -d ' ' -f2-` karena masih print kolom 9 dan 2, maka gunakan cut untuk memisahkan elemen berdasarkan spasi `' '` dan hapus elemen pertama

**Output**

![image](https://user-images.githubusercontent.com/91055469/224313343-1488becf-f38c-4001-bd18-ee9337b48602.png)

<br/>

### 1.3 10 Universitas dengan GER Rank tertinggi di Jepang

Karena Bocchi takut salah membaca ketika memasukkan nama universitas, cari 10 Universitas di Jepang dengan Employment Outcome Rank(ger rank) paling tinggi.

**Code**

```sh
awk '/Japan/ {print}' 2023\ QS\ World\ University\ Rankings.csv | awk -F ',' '{print $20,$2}' | sort -n | head -n 10 | cut -d ' ' -f2-
```

- `awk '/Japan/ {print}' 2023\ QS\ World\ University\ Rankings.csv` Pertama gunakan awk untuk mendapatkan content di file yang hanya mengandung kata `Japan`
- `awk -F ',' '{print $20,$2}'` setelah gunakan pipeline `|` gunakan `awk` untuk memisahkan indeks berdasarkan `,` dan gunakan `{print $20, $2}` untuk mendapatkan hanya kolom ke - 20 dan kolom ke - 2
- `sort -n` gunakan sort untuk mengurutkan berdasarkan indeks pertama yang di print yaitu indeks ke - 20
- `head -n 10` gunakan head untuk mendapatkan 10 elemen teratas
- `cut -d ' ' -f2-` karena masih print kolom 20 dan 2, maka gunakan cut untuk memisahkan elemen berdasarkan spasi `' '` dan hapus elemen pertama

**Output**

![image](https://user-images.githubusercontent.com/91055469/224313468-c7b37384-4477-4d48-a7e0-9076c172bd59.png)

### 1.4 Universitas yang memiliki kata kunci `Keren`

Bocchi ngefans berat dengan universitas paling keren di dunia. Bantu bocchi mencari universitas tersebut dengan kata kunci keren.

**Code**

```sh
awk '/Keren/ {print}' 2023\ QS\ World\ University\ Rankings.csv | awk -F ',' '{print $2}'
```

- `awk '/Keren/ {print}' 2023\ QS\ World\ University\ Rankings.csv` Pertama gunakan awk untuk mendapatkan content di file yang hanya mengandung kata `Keren`
- `awk -F ',' '{print $2}'` setelah gunakan pipeline `|` gunakan `awk` untuk memisahkan indeks berdasarkan `,` dan gunakan `{print $2}` untuk mendapatkan hanya kolom ke - 2 saja

**Output**

![image](https://user-images.githubusercontent.com/91055469/224313569-368f0fe3-8f3b-4f39-a32e-b48501ef9c0e.png)

#### Kendala
Masih belum ditemukan kendala dalam mengerjakan soal nomor 1, karena syntax untuk mengerjakan sudah tersedia di modul

## Soal 2

Kobeni ingin pergi ke negara terbaik di dunia bernama Indonesia. Akan tetapi karena uang Kobeni habis untuk beli headphone ATH-R70x, Kobeni tidak dapat melakukan hal tersebut.

2.1 Untuk melakukan coping, Kobeni mencoba menghibur diri sendiri dengan mendownload gambar tentang Indonesia. Coba buat script untuk mendownload gambar sebanyak X kali dengan X sebagai jam sekarang (ex: pukul 16:09 maka X nya adalah 16 dst. Apabila pukul 00:00 cukup download 1 gambar saja). Gambarnya didownload setiap 10 jam sekali mulai dari saat script dijalankan. Adapun ketentuan file dan folder yang akan dibuat adalah sebagai berikut:

- File yang didownload memilki format nama “perjalanan_NOMOR.FILE” Untuk NOMOR.FILE, adalah urutan file yang download (perjalanan_1, perjalanan_2, dst)
- File batch yang didownload akan dimasukkan ke dalam folder dengan format nama “kumpulan_NOMOR.FOLDER” dengan NOMOR.FOLDER adalah urutan folder saat dibuat (kumpulan_1, kumpulan_2, dst)

  2.2 Karena Kobeni uangnya habis untuk reparasi mobil, ia harus berhemat tempat penyimpanan di komputernya. Kobeni harus melakukan zip setiap 1 hari dengan nama zip “devil_NOMOR ZIP” dengan NOMOR.ZIP adalah urutan folder saat dibuat (devil_1, devil_2, dst). Yang di ZIP hanyalah folder kumpulan dari soal di atas.

### 2.1 Mendownload File secara Otomatis

Pada soal 2.1 diberikan case untuk membuat folder tiap 10 jam sekali secara otomatis. Selain itu, program yang dibuat juga dapat mendownload file sebanyak jam terkini yang akan dimasukkan ke dalam folder tersebut. Misal, sekarang menunjukkan pukul 9:11 maka file yang akan terdownload ada 9 file. Program ini dibuat dengan ketentuan :

1. Nama folder : kumpulan_counter
2. Nama file : perjalanan_counter

Untuk menyelesaikan case tersebut, langkah pertama yang dilakukan adalah mengecek ada/tidak folder 'kumpulan' dengan menggunakan code berikut.

```sh
temp1=1
while [ -d "kumpulan_$temp1" ]
do temp1=$((temp1+1))
done
```

disini counter dari folder 'kumpulan' disimpan pada temp1, temp1 akan terus bertambah setiap program dieksekusi

Kemudian jika tidak ada folder 'kumpulan', dibuat sebuah fungsi bernama 'kumpulanDownlaod'. Di dalam fungsi ini folder akan dibuat dibuat terlebih dahulu dengan menggunakan command `mkdir` seperti berikut.

```sh
mkdir "kumpulan_$temp"
```

Kemudian mengambil jam terkini dengan menggunakan code seperti berikut.

```sh
hour=$(date +"%H")
```

Setelah waktu didapatkan, maka dapat mendownload file sebanyak waktu terkini. Oleh karena itu, dibutukan looping sesuai waktu yang telah didapatkan seperti code berikut.

```sh
# looping sesuai waktu
for ((i=1; i<=$hour; i++))
do
    # download gambar random dan disimpan dalam folder baru
    wget -O "kumpulan_$temp/perjalanan_$i.jpg" "https://source.unsplash.com/random/800x600/?indonesia"
done
```

Dari code diatas dapat dilihat, untuk mendownload file dari suatu URL menggunakan command `wget`. Untuk eksekusi pertama, folder yang dibuat diberi nama kumpulan_1 didalam folder kumpulan_1 terdapat file-file yang telah didownload bernama perjalanan_1, perjalanan_2, perjalanan_3, dst sesuai jam terkini.

Kemudian menggunakan cron job untuk memungkinkan folder dan file dapat dieksekusi tiap 10 jam sekali secara otomatis.

```sh
* */10 * * * bash $PWD/kobeni_liburan.sh 1
```

Cron job akan menjalankan file kobeni_liburan.sh dengan argumen 1 yang mana akan memanggil fungsi kumpulanDownload.

Berikut keadaan terminal setelah menjalankan file kobeni_liburan.sh dengan argumen 1

![1_-_Download_File](/uploads/3830e6878deb4d659bef89ca6f8c113c/1_-_Download_File.png)

File yang didownload sampai perjalanan_10, karena program dijalankan saat jam 10 pagi.

Berikut bukti folder kumpulan_1 sudah dibuat.

![2_-_dir_kumpulan](/uploads/e72af8ab56c3a32feaf1c9aa8fd756a9/2_-_dir_kumpulan.png)

Berikut bukti cron job yang sedang berjalanan.

![5_-_cron_job](/uploads/be3d6d0124e71790e7dd57a62393107a/5_-_cron_job.png)

### 2.2 Zip Folder secara Otomatis

Pada soal 2.2 diberikan case untuk melakukan zip pada folder 'kumpulan' yang ada setiap 1 hari sekali secara otomatis. Folder zip ini diberi nama devil_counter.

Untuk menyelesaikan case tersebut, langkah pertama yang dilakukan adalah mengecek ada/tidak folder zip bernama 'devil' dengan menggunakan code berikut.

```sh
temp2=1
while [ -f "devil_$temp2.zip" ]
do temp2=$((temp2+1))
done
```

disini counter dari folder zip 'devil' disimpan pada temp2, temp2 akan terus bertambah setiap program dieksekusi

Kemudian jika tidak ada folder zip 'devil', dibuat sebuah fungsi bernama 'devilZip'. Di dalam fungsi ini folder 'kumpulan' akan dikompres secara rekursif dengan menggunakan command `zip` dan argumen tambahan `r` seperti berikut.

```sh
zip -r "devil_$temp2.zip" "kumpulan_$kumpulanTemp"
```

Kemudian menghapus folder yang telah di zip dengan menggunakan command `rm` seperti berikut.

```sh
rm -rf "kumpulan_$kumpulanTemp"
```

Kemudian menggunakan cron job untuk memungkinkan folder dapat di zip tiap 1 hari sekali secara otomatis.

```sh
0 0 * * * bash $pwd/kobeni_liburan.sh 2
```

Cron job akan menjalankan file kobeni_liburan.sh dengan argumen 2 yang mana akan memanggil fungsi devilZip.

Berikut keadaan terminal setelah menjalankan file kobeni_liburan.sh dengan argumen 2.

![3_-_Zip_File](/uploads/75c50fe088dfa54cdd81b60d4c2c25be/3_-_Zip_File.png)

Berikut bukti folder kumpulan_1 telah di zip dan diberi nama devil_1. Folder kumpulan_1 juga telah dihapus.

![4_-_dir_devil](/uploads/0b77d0e6338858db8a8d28dd0268abef/4_-_dir_devil.png)

Berikut bukti cron job yang sedang berjalanan.

![5_-_cron_job](/uploads/be3d6d0124e71790e7dd57a62393107a/5_-_cron_job.png)

### Kendala Pengerjaan

Kendala yang dialami saat mengerjakan soal nomor 2:

1. File yang telah didownload berada diluar folder 'kumpulan'
2. Terjadi kesalahan syntax pada saat membuat program cron job sehingga cron job tidak bisa berjalan

## Soal 3

### 3.1 Deskripsi Problem

Peter Griffin hendak membuat suatu sistem register pada script `louis.sh` dari setiap user yang berhasil didaftarkan di dalam file `/users/users.txt`. Peter Griffin juga membuat sistem login yang dibuat di script `retep.sh`.

Untuk memastikan password pada register dan login aman, maka ketika proses input passwordnya harus memiliki ketentuan berikut.
- Minimal 8 karakter
- Memiliki minimal 1 huruf kapital dan 1 huruf kecil
- *Alphanumeric*
- Tidak boleh sama dengan username
- Tidak boleh menggunakan kata `chicken` atau `ernie`

Setiap percobaan login dan register akan tercatat pada log.txt dengan format : `YY/MM/DD hh:mm:ss MESSAGE`. Message pada log akan berbeda tergantung aksi yang dilakukan user.
- Ketika mencoba register dengan username yang sudah terdaftar, maka message pada log adalah `REGISTER: ERROR User already exists`
- Ketika percobaan register berhasil, maka message pada log adalah `REGISTER: INFO User USERNAME registered successfully`
- Ketika user mencoba login namun passwordnya salah, maka message pada log adalah `LOGIN: ERROR Failed login attempt on user USERNAME`
- Ketika user berhasil login, maka message pada log adalah `LOGIN: INFO User USERNAME logged in`

### 3.2 Membuat Script Register

Pertama, kita harus memastikan terlebih dahulu bahwa harus terdapat direktori dan file *database* registrasi user data pada direktori root (`/`). Jika belum terdapat direktori dan file tersebut, maka buat direktori dan file tersebut. Berikut adalah implementasi skrip *shell*-nya.

```bash
if [ ! -d /users ]; then
    mkdir /users
fi
if [ ! -f /users/users.txt ]; then
    touch /users/users.txt
fi
```

Setelah itu, kita akan meminta input dari user berupa username dan password yang akan didaftarkan. Dalam hal ini, kita harus mengecek apakah username yang diinputkan user sudah terdapat di file `users.txt` atau belum. Jika sudah ada, maka tampilkan *error message* pada terminal dan catat log kegagalannya pada file `log.txt`. Jika belum terdapat username yang sama, maka lanjut ke proses berikutnya.

```bash
while grep -q "^$user_name:" /users/users.txt; do
    echo "$(date +"%y/%m/%d %H:%M:%S") REGISTER: ERROR User already exists" >> log.txt
    echo "> Username already registered"
    echo -n "Enter new username: "
    read user_name
done
```

Langkah selanjutnya adalah melakukan validasi password yang user daftarkan sesuai kriteria pada soal. Apabila password tidak memenuhi kriteria yang ada, tampilkan *error message*.
- Minimal 8 karakter
```bash
if [ ${#user_password} -lt 8 ]; then
    echo "> Password must be at least 8 characters long"
    is_valid=false
fi
```
- Memiliki minimal 1 huruf kapital dan 1 huruf kecil (serta memastikan setidaknya ada 1 angka)
```bash
if ! [[ "$user_password" =~ [[:upper:]] ]] || ! [[ "$user_password" =~ [[:lower:]] ]] || ! [[ "$user_password" =~ [[:digit:]] ]]; then
    echo "> Password must contain at least 1 uppercase letter, 1 lowercase letter, and 1 number"
    is_valid=false
fi
```
- *Alphanumeric*
```bash
if ! [[ "$user_password" =~ ^[[:alnum:]]+$ ]]; then
    echo "> Password must contain only alphanumeric characters"
    is_valid=false
fi
```
- Tidak boleh sama dengan username
```bash
if [ "$user_password" == "$user_name" ]; then
    echo "> Password must not be the same as the username"
    is_valid=false
fi
```
- Tidak boleh menggunakan kata `chicken` atau `ernie`
```bash
if [[ "$user_password" =~ [Cc][Hh][Ii][Cc][Kk][Ee][Nn] ]] || [[ "$user_password" =~ [Ee][Rr][Nn][Ii][Ee] ]]; then
    echo "> Password must not contain the word: chicken or ernie"
    is_valid=false
fi
```

Jika username dan password yang diinputkan oleh user sudah valid dan memenuhi semua kriteria, daftarkan pada `/users/users.txt` dan catat log keberhasilannya di `log.txt`.
```bash
echo "$user_name:$user_password" >> /users/users.txt

echo "$(date +"%y/%m/%d %H:%M:%S") REGISTER: INFO User $user_name registered successfully" >> log.txt
echo "> User registered successfully."
```

### 3.3 Membuat Script Login

Pertama, kita akan meminta input username dan password kepada user yang telah terdaftar akunnya.
```bash
echo -n "Enter username: "
read user_name

echo -n "Enter password: "
read -s user_password
```

Setelah itu, kita akan melakukan pengecekan dan validasi apakah username dan password yang telah diinputkan user telah terdaftar di `users.txt`. Jika sudah, tampilakan *success message* dan catat log keberhasilannya di `log.txt`. Jika belum terdaftar atau mungkin saja user melakukan kesalahan saat menginputkan username dan password sehingga tidak ditemukan kecocokan dengan data pada `users.txt`, tampilkan *failure message* dan catat log kegagalannya di `log.txt`. Berikut adalah implementasi skrip shell-nya.
```bash
if grep -q "^$user_name:$user_password" /users/users.txt; then
	echo "$(date +"%y/%m/%d %H:%M:%S") LOGIN: INFO User $user_name logged in" >> log.txt
	echo "> Successfully logged in. Welcome, $user_name!"
else
	echo "$(date +"%y/%m/%d %H:%M:%S") LOGIN: ERROR Failed login attempt on user $user_name" >> log.txt
	echo "> Login failed. Make sure your username and password are correct"
fi
```

### 3.4 Contoh Output

Beberapa contoh output eksekusi skrip sebagai berikut.

1. Registrasi sukses
![success-registration](https://user-images.githubusercontent.com/107914177/224336208-ae7bbdc4-7ae4-4218-a7c5-d9fb0d3f7264.png)
2. Registrasi gagal karena username telah terdaftar
![username-registered](https://user-images.githubusercontent.com/107914177/224336211-15713d7a-5970-473b-861f-236f430c5704.png)
3. Registrasi gagal karena password tidak memenuhi kriteria
![password-not-valid](https://user-images.githubusercontent.com/107914177/224336201-033fe7fa-ee0c-4fe1-a712-1c6c3c8c5f59.png)
4. Login sukses
![success-login](https://user-images.githubusercontent.com/107914177/224336204-39da9c20-b864-4aaf-815e-075b0c9a4bc8.png)
5. Login gagal
![failed-login](https://user-images.githubusercontent.com/107914177/224336192-63900b3f-09a0-410b-8bfa-0158424eb55a.png)
6. File `users.txt`
![users](https://user-images.githubusercontent.com/107914177/224336216-66fd6453-7b15-486e-bf24-352598f390a5.png)
7. File `log.txt`
![log](https://user-images.githubusercontent.com/107914177/224336194-21723a2a-0e71-493f-a9f3-ab9fa483e8cf.png)


### 3.5 Kendala

Tidak ada kendala yang berarti saat mengerjakan problem ini. Namun, perlu digarisbawahi bahwa eksekusi skrip `louis.sh` dan `retep.sh` harus dilakukan dalam mode *superuser* karena dalam eksekusinya kedua skrip ini mengambil dan memodifikasi data yang terdapat dalam direktori root (`/`). Jika tidak dilakukan demikian, maka akan terjadi galat sebagai berikut.
![failed-exe](https://user-images.githubusercontent.com/107914177/224336185-bfee6d3f-dc1c-4e36-8301-29a05a00e95b.png)

## Soal 4

Johan Liebert adalah orang yang sangat kalkulatif. Oleh karena itu ia mengharuskan dirinya untuk mencatat log system komputernya. File syslog tersebut harus memiliki ketentuan :

4.1

Backup file log system dengan format jam:menit tanggal:bulan:tahun (dalam format .txt).
<br />
Isi file harus dienkripsi dengan string manipulation yang disesuaikan dengan jam dilakukannya backup seperti berikut:
Menggunakan sistem cipher dengan contoh seperti berikut. Huruf b adalah alfabet kedua, sedangkan saat ini waktu menunjukkan pukul 12, sehingga huruf b diganti dengan huruf alfabet yang memiliki urutan ke 12+2 = 14
Hasilnya huruf b menjadi huruf n karena huruf n adalah huruf ke empat belas, dan seterusnya.
Setelah huruf z akan kembali ke huruf a

**Code**

```sh
sudo mkdir encrypt_logs

temp="$(date +"%H:%M %d:%m:%y").txt"

sudo touch encrypt_logs/"$temp"
hour=$(date +"%H")
alphabetLower="abcdefghijklmnopqrstuvwxyz"
alphabetUpper="ABCDEFGHIJKLMNOPQRSTUVWXYZ"

$(cat /var/log/syslog | tr "${alphabetLower:0:26}" "${alphabetLower:$hour:26}${alphabetLower:0:$hour}" |  tr "${alphabetUpper:0:26}" "${alphabetUpper:$hour:26}${alphabetUpper:0:$hour}" > "encrypt_logs/$temp")
```

- `sudo mkdir encrypt_logs` Membuat directory baru untuk menyimpan backup log file
- `temp="$(date +"%H:%M %d:%m:%y").txt"` Deklarasi nama file .txt sesuai dengan ketentuan di soal yaitu 'jam:menit tanggal:bulan:tahun.txt'
- `sudo touch encrypt_logs/"$temp"` Membuat file .txt baru
- `hour=$(date +"%H")` Deklarasi jam sekarang untuk menentukan berapa kali huruf akan di geser
- `alphabetLower="abcdefghijklmnopqrstuvwxyz"` Deklarasi semua alphabet huruf kecil `alphabetUpper="ABCDEFGHIJKLMNOPQRSTUVWXYZ"` Deklarasi semua alphabet huruf besar
- `cat /var/log/syslog` untuk pendapatkan semua isi file dari syslog
- `tr "${alphabetLower:0:26}" "${alphabetLower:$hour:26}${alphabetLower:0:$hour}"` menggunakan syntax `tr` untuk melakukan translasi pada string, selanjutnya parameter pertama `tr` yaitu `"${alphabetLower:0:26}"` mengambil semua karakter pada variabel `alphabetLower` dari index `0 - 26`, selanjutnya parameter kedua `tr` yaitu `"${alphabetLower:$hour:26}${alphabetLower:0:$hour}"` berisi karakter pengganti parameter pertama, karakter pengganti diambil dari `index ke - hour` hingga `index ke - hour + 26` kemudian digabungkan dengan karakter pada `index ke - 0` hingga `index ke - hour - 1`, sederhananya digunakan untuk mengganti setiap char pada string dengan karakter pada variabel `alphabetLower` sejauh `hour`
- `tr "${alphabetUpper:0:26}" "${alphabetUpper:$hour:26}${alphabetUpper:0:$hour}"` sama seperti fungsi diatas namun digunakan untuk alphabet dengan huruf besar
- `> "encrypt_logs/$temp"` untuk memasukkan hasil operasi sebelumnya kedalam file `$temp`

**Output**

![image](https://user-images.githubusercontent.com/91055469/224313946-22436685-b0fb-45db-b564-74d624ab797c.png)

4.2

Buat juga script untuk dekripsinya.

**Code**

```sh
    sudo mkdir decrypt_logs
    cd encrypt_logs

    for file in *.txt; do
        cd ..
        sudo touch decrypt_logs/"$file"
        hour=$(echo $file | cut -c-2)
        alphabetLower="abcdefghijklmnopqrstuvwxyz"
        alphabetUpper="ABCDEFGHIJKLMNOPQRSTUVWXYZ"
        decrypt=$(cat encrypt_logs/"$file" | tr "${alphabetLower:$hour:26}${alphabetLower:0:$hour}" "${alphabetLower:0:26}" |  tr "${alphabetUpper:$hour:26}${alphabetUpper:0:$hour}" "${alphabetUpper:0:26}" > "decrypt_logs/$file")
        cd decrypt_logs
    done
```

- `sudo mkdir decrypt_logs` Membuat directory baru untuk menyimpan semua file hasil decrypt
- `cd encrypt_logs` Pergi ke dalam folder encrypt untuk mendapatkan konten file .txt
- `for file in *.txt; do` Gunakan looping untuk setiap isi file .txt di dalam folder encrypt
- `cd ..` Keluar dari file encrypt
- `sudo touch decrypt_logs/"$file"` Buat file baru untuk menyimpan hasil decrypt sesuai dengan nama file encrypt
- `hour=$(echo $file | cut -c-2)` Deklarasi jam sesuai file encrypt
- `alphabetLower="abcdefghijklmnopqrstuvwxyz"` Deklarasi alphabet huruf kecil
- `alphabetUpper="ABCDEFGHIJKLMNOPQRSTUVWXYZ"` Deklarasi alphabet huruf besar
- `decrypt=$(cat encrypt_logs/"$file" | tr "${alphabetLower:$hour:26}${alphabetLower:0:$hour}" "${alphabetLower:0:26}" |  tr "${alphabetUpper:$hour:26}${alphabetUpper:0:$hour}" "${alphabetUpper:0:26}" > "decrypt_logs/$file")` Sama seperti encrypt hanya dibalik parameter 1 dan 2 nya, mengambil semua isi file encrypt kemudian di shift secara terbalik dan dimasukkan kedalam file decrypt
- `cd decrypt_logs` masuk kedalam file decrypt

**Output**

![image](https://user-images.githubusercontent.com/91055469/224314088-7a260a37-38f4-49e8-860a-bae46f9c1f28.png)

<br />

4.3

Backup file syslog setiap 2 jam untuk dikumpulkan
**Code**

```sh
# Crontab
0 */2 * * * /bin/bash /home/rencist/Downloads/Praktikum1/sisop-praktikum-modul-1-2023-bj-a07/soal4/log_encrypt.sh
```

- `0 */2 * * *` Crontab dijalankan setiap 2 jam
- `/bin/bash /home/rencist/Downloads/Praktikum1/sisop-praktikum-modul-1-2023-bj-a07/soal4/log_encrypt.sh` Cronjob akan menjalankan bash `log_encrypt.sh`

**Cronjob**

![image](https://user-images.githubusercontent.com/91055469/224314214-410f08ff-ef9f-4e47-bab3-3661830b1267.png)

_Kendala_

Tidak ditemukan kendala dalam pengerjaan praktikum soal nomor 4
