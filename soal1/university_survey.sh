#!/bin/bash

echo "a. 5 Universitas Ranking Tertinggi di Jepang :"
awk '/Japan/ {print}' 2023\ QS\ World\ University\ Rankings.csv | cut -d ',' -f 2 | head -n 5
echo -e
echo "b. 5 Universitas Faculty Student Score Yang Paling Rendah :"
awk '/Japan/ {print}' 2023\ QS\ World\ University\ Rankings.csv | awk -F ',' '{print $9,$2}' | sort -n | head -5 | cut -d ' ' -f2-
echo -e
echo "c. 10 Universitas di Jepang Dengan Employment Outcome Rank paling tinggi :"
awk '/Japan/ {print}' 2023\ QS\ World\ University\ Rankings.csv | awk -F ',' '{print $20,$2}' | sort -n | head -n 10 | cut -d ' ' -f2-
echo -e
echo "d. Universitas Paling Keren di Dunia :"
awk '/Keren/ {print}' 2023\ QS\ World\ University\ Rankings.csv | awk -F ',' '{print $2}'
